#include <iostream>
#include "Utils/FirstUtil.cpp"
#include "Entitys/HPItemEntity.h"
#include "Entitys/HPUtils.h"
#include "Entitys/HPEnumDirection.h"
#include "Entitys/HPClustering.h"

using namespace std;


int main() {

    int popSize = 10;
    int subPopSize = 5;
    double pm=0.05;
    vector<enumNamespace::HPModel> s = HPUtils::redFromFile("D:\\example2.txt");
    vector<HPItemEntity> arrayAll(popSize, HPItemEntity(s.size(), 0.0));
    HPUtils::initDirectionsPopulation(arrayAll);
    vector<vector<HPItemEntity>> subPop;
    vector<HPItemEntity> tmpPop;

    int maxRuns = 0;

    do {
        subPop = HPClustering::clustering(arrayAll, popSize, subPopSize);
        for (int i = 0; i < subPop.size(); i++) {
            for (int j = 0; j < subPop[i].size(); j++) {
                HPUtils::checkWalk(subPop[i][j], s);
            }
        }

        for (int i = 0; i < subPop.size(); i++) {
            HPUtils::corssoverMutation(subPop[i], tmpPop);
            HPUtils::checkWalk(tmpPop[tmpPop.size() - 2], s);
            HPUtils::checkWalk(tmpPop[tmpPop.size() - 1], s);
        }
        tmpPop.clear();
        maxRuns++;
        cout << "Finished sequance:" << maxRuns<<endl;
        HPUtils::inverseHPSequence(s);
        arrayAll = HPUtils::oppositeInverseIndividual(arrayAll);
    } while (maxRuns != 10);

    for (int i = 0; i < subPop.size(); i++) {
        HPUtils::corssoverMutation(subPop[i], tmpPop);
        HPUtils::checkWalk(tmpPop[tmpPop.size() - 2], s);
        HPUtils::checkWalk(tmpPop[tmpPop.size() - 1], s);
    }
//    HPItemEntity nearestItem;
//    for (int i = 0; i < tmpPop.size(); i++) {
//        nearestItem = HPUtils::nerest(tmpPop[i], arrayAll);
//    }


    for (int i = 0; i < subPop.size(); i++) {
        for (int j = 0; j < subPop[i].size(); j++) {
            cout << "Fitness : " << subPop[i][j].fitness << endl;
        }
    }

    cout<<"Test42234";
    return 0;
}