//
// Created by Primoz-PC on 12. 09. 2016.
//

#ifndef PERNAT_MAGISTRSKA_HPUTILS_H
#define PERNAT_MAGISTRSKA_HPUTILS_H

#include <vector>
#include <random>
#include "HPCoordinate.h"
#include "HPItemEntity.h"
#include "HPPoints.h"
#include <string>
#include <unordered_map>


union Point {
    Point() { key = 0; }

    inline bool operator==(const Point &p) const { return key == p.key; }

    inline bool operator<(const Point &p) const { return key < p.key; }

    inline bool operator!=(const Point &p) const { return key != p.key; }

    inline const int16_t &operator[](const int16_t idx) const { return coord[idx]; }

    inline int16_t &operator[](const int16_t idx) { return coord[idx]; }

    int16_t coord[4];
    int64_t key;
};

class HPUtils {
public:
    void static setDirectionVector(HPCoordinate &hpCoordinate);

    vector<enumNamespace::Direction> static getDirectionVector(enumNamespace::Direction direction);

    vector<enumNamespace::Direction> static generateDirectionVector();

    vector<enumNamespace::HPModel> static generateHPVector(int size);

    vector<HPItemEntity> static oppositeInverseIndividual(vector<HPItemEntity> pop);

    void static inverseHPSequence(vector<enumNamespace::HPModel> s);

    vector<enumNamespace::HPModel> static redFromFile(std::string filePath);

    void static initDirectionsPopulation(vector<HPItemEntity> &arrayAll);

    void static checkWalk(HPItemEntity &hpItemEntity, vector<enumNamespace::HPModel> s);

    static HPPoints getNextMove(enumNamespace::Direction direction);

    static void eval(HPItemEntity &tmp, vector<enumNamespace::HPModel> s);

    static void mutation(HPItemEntity &tmp);

    static void corssoverMutation(vector<HPItemEntity> pop, vector<HPItemEntity> &tmpPop);

    static HPItemEntity nerest(HPItemEntity tmpItem, vector<HPItemEntity> &tmpPop);

private:
    static bool checkIfNeighbours(Point startPoitn, Point newPoint);

    static Point calcNewLocation(Point point, enumNamespace::Direction direction);

    static void removeDirection(vector<enumNamespace::Direction> &tmp, enumNamespace::Direction direction);

    static void treePointCrossover(HPItemEntity &first, HPItemEntity &second);
};


#endif //PERNAT_MAGISTRSKA_HPUTILS_H
