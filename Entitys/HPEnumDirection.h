//
// Created by Primoz-PC on 12. 09. 2016.
//

#ifndef PERNAT_MAGISTRSKA_HPENUMDIRECTION_H
#define PERNAT_MAGISTRSKA_HPENUMDIRECTION_H

namespace enumNamespace {
    enum Direction {
        S, W, N, E, NWp, NWm, NEp, NEm, SEp, SWp, SEm, SWm, empty
    };
    enum HPModel{
        H ,P
    };
}

#endif //PERNAT_MAGISTRSKA_HPENUMDIRECTION_H
