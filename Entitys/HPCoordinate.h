//
// Created by Primoz-PC on 12. 09. 2016.
//

#ifndef PERNAT_MAGISTRSKA_HPCOORDINATE_H
#define PERNAT_MAGISTRSKA_HPCOORDINATE_H

#include <vector>
#include "HPEnumDirection.h"

using namespace std;

class HPCoordinate {
public:
    inline bool operator==(const HPCoordinate &p) const { return direction == p.direction; }

    int x;
    int y;
    int z;
    enumNamespace::Direction direction;

    HPCoordinate();

    HPCoordinate(int x, int y, int z);

    HPCoordinate(int myArray);
};


#endif //PERNAT_MAGISTRSKA_HPCOORDINATE_H
