//
// Created by Primoz-PC on 12. 09. 2016.
//

#include "HPItemEntity.h"

using namespace std;

HPItemEntity::HPItemEntity(int arraySize, double fitness) : fitness(fitness) {
    this->vector1.assign(arraySize, HPCoordinate());
    fitness = 0;
    distance = 0;
}

double HPItemEntity::getFitness() const {
    return fitness;
}

void HPItemEntity::setFitness(double fitness) {
    HPItemEntity::fitness = fitness;
}

HPItemEntity::HPItemEntity() {

}

bool HPItemEntity::operator<(const HPItemEntity &other) const{
    return distance > other.distance;
}
