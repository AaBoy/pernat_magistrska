//
// Created by Primoz-PC on 25. 09. 2016.
//

#ifndef PERNAT_MAGISTRSKA_HPPOINTS_H
#define PERNAT_MAGISTRSKA_HPPOINTS_H


class HPPoints {
public:
    int x, z, y;
    int key;

    HPPoints();
    HPPoints(int x, int z, int y);
    bool operator<(const HPPoints& src)const
    {
        return (src.key < key);
    }

    bool operator==(const HPPoints& p) const { return key == p.key; }
    bool operator!=(const HPPoints& p) const { return key != p.key; }
};


#endif //PERNAT_MAGISTRSKA_HPPOINTS_H
