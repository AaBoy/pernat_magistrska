//
// Created by Primoz-PC on 16. 09. 2016.
//
#include<cstdlib>
#include<ctime>
#include <algorithm>
#include "HPClustering.h"

vector<vector<HPItemEntity>> HPClustering::clustering(vector<HPItemEntity> p, int popSize, int subPopSize) {
    vector<HPItemEntity> t = p;
    HPItemEntity tr;
    vector<vector<HPItemEntity>> C;
    int random = -1;
    srand( static_cast<unsigned int>(time(NULL)));
    for (int i = 0; i < popSize / subPopSize; i++) {
        random = rand() % t.size();
        tr = t[random];
        t.erase(t.begin() + random);

        C.push_back(nearest(t, tr, subPopSize - 1));
    }
    return C;
}

vector<HPItemEntity>  HPClustering::nearest(vector<HPItemEntity> &t, HPItemEntity tr, int subPopSize) {
    vector<HPItemEntity> C;

    for (int i = 0; i < t.size(); i++) {
        t[i].distance = 0;
        for (int j = 0; j < tr.vector1.size(); j++) {
            if (tr.vector1[j].direction == t[i].vector1[j].direction) {
                t[i].distance++;
            }
        }
    }

    sortPopDistance(t);

    C.push_back(tr);
    do {
        C.push_back(t[0]);
        t.erase(t.begin());

        if (t.size() == 0) {
            break;
        }
    } while (C.size() != subPopSize + 1);

    return C;

}

void HPClustering::sortPopDistance(vector<HPItemEntity> &t){
    sort(t.begin(), t.end());
}

