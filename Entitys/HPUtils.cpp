//
// Created by Primoz-PC on 12. 09. 2016.
//

#include "HPUtils.h"
#include "HPPoints.h"
#include "HPEnumDirection.h"
#include<iostream>
#include<fstream>
#include<cstdlib>
#include<ctime>
#include <cstdlib>
#include <map>
#include <unordered_map>
#include <functional>
#include <algorithm>

void HPUtils::setDirectionVector(HPCoordinate &hpCoordinate) {
    switch (hpCoordinate.direction) {
        case enumNamespace::N:
            hpCoordinate.x = 1;
            hpCoordinate.y = 1;
            hpCoordinate.z = 0;
            break;
        case enumNamespace::S:
            hpCoordinate.x = -1;
            hpCoordinate.y = -1;
            hpCoordinate.z = 0;
            break;
        case enumNamespace::W:
            hpCoordinate.x = -1;
            hpCoordinate.y = 1;
            hpCoordinate.z = 0;
            break;
        case enumNamespace::E:
            hpCoordinate.x = 1;
            hpCoordinate.y = -1;
            hpCoordinate.z = 0;
            break;
        case enumNamespace::NWp:
            hpCoordinate.x = 0;
            hpCoordinate.y = 1;
            hpCoordinate.z = 1;
            break;
        case enumNamespace::NWm:
            hpCoordinate.x = 0;
            hpCoordinate.y = 1;
            hpCoordinate.z = -1;
            break;
        case enumNamespace::NEp:
            hpCoordinate.x = 1;
            hpCoordinate.y = 0;
            hpCoordinate.z = 1;
            break;
        case enumNamespace::NEm:
            hpCoordinate.x = 1;
            hpCoordinate.y = 0;
            hpCoordinate.z = -1;
            break;
        case enumNamespace::SEp:
            hpCoordinate.x = 0;
            hpCoordinate.y = -1;
            hpCoordinate.z = 1;
            break;
        case enumNamespace::SEm:
            hpCoordinate.x = 0;
            hpCoordinate.y = -1;
            hpCoordinate.z = -1;
            break;
        case enumNamespace::SWp:
            hpCoordinate.x = -1;
            hpCoordinate.y = 0;
            hpCoordinate.z = 1;
            break;
        case enumNamespace::SWm:
            hpCoordinate.x = -1;
            hpCoordinate.y = 0;
            hpCoordinate.z = -1;
            break;
    }
}

vector<enumNamespace::Direction> HPUtils::getDirectionVector(enumNamespace::Direction direction) {

    vector<enumNamespace::Direction> tmp = generateDirectionVector();
    switch (direction) {
        case enumNamespace::N:
            tmp.erase(tmp.begin() + 1);
            break;
        case enumNamespace::S:
            tmp.erase(tmp.begin() + 0);
            break;
        case enumNamespace::W:
            tmp.erase(tmp.begin() + 3);
            break;
        case enumNamespace::E:
            tmp.erase(tmp.begin() + 2);
            break;
        case enumNamespace::NWp:
            tmp.erase(tmp.begin() + 9);
            break;
        case enumNamespace::NWm:
            tmp.erase(tmp.begin() + 8);
            break;
        case enumNamespace::NEp:
            tmp.erase(tmp.begin() + 11);
            break;
        case enumNamespace::NEm:
            tmp.erase(tmp.begin() + 10);
            break;
        case enumNamespace::SEp:
            tmp.erase(tmp.begin() + 5);
            break;
        case enumNamespace::SEm:
            tmp.erase(tmp.begin() + 4);
            break;
        case enumNamespace::SWp:
            tmp.erase(tmp.begin() + 7);
            break;
        case enumNamespace::SWm:
            tmp.erase(tmp.begin() + 6);
            break;
    }
    return tmp;
}

vector<enumNamespace::Direction> HPUtils::generateDirectionVector() {
    vector<enumNamespace::Direction> tmp(12);
    tmp[0] = enumNamespace::N;
    tmp[1] = enumNamespace::S;
    tmp[2] = enumNamespace::W;
    tmp[3] = enumNamespace::E;
    tmp[4] = enumNamespace::NWp;
    tmp[5] = enumNamespace::NWm;
    tmp[6] = enumNamespace::NEp;
    tmp[7] = enumNamespace::NEm;
    tmp[8] = enumNamespace::SEp;
    tmp[9] = enumNamespace::SEm;
    tmp[10] = enumNamespace::SWp;
    tmp[11] = enumNamespace::SWm;
    return tmp;
}

vector<enumNamespace::HPModel> HPUtils::generateHPVector(int size) {
    std::random_device rd;
    std::mt19937_64 rng(rd());
    std::uniform_int_distribution<int> uni(0, 100);

    vector<enumNamespace::HPModel> test(size);

    for (int i = 0; i < size; i++) {
        if (uni(rng) > 50) {
            test[i] = enumNamespace::H;
        } else {
            test[i] = enumNamespace::P;
        }
    }

    return test;
}

vector<HPItemEntity> HPUtils::oppositeInverseIndividual(vector<HPItemEntity> pop) {
    int vectorSize = pop.size();
    vector<HPItemEntity> tmp(vectorSize);
    int directionSize = pop[0].vector1.size();

    for (int i = 0; i <pop.size(); i++) {
        tmp[i] = HPItemEntity(directionSize, 0);
        vectorSize = directionSize - 1;

        for (int j = directionSize - 1; j >= 0; j--) {
            switch (pop[i].vector1[j].direction) {
                case enumNamespace::N:
                    tmp[i].vector1[vectorSize - j].direction = enumNamespace::S;
                    break;
                case enumNamespace::S:
                    tmp[i].vector1[vectorSize - j].direction = enumNamespace::N;
                    break;
                case enumNamespace::W:
                    tmp[i].vector1[vectorSize - j].direction = enumNamespace::E;
                    break;
                case enumNamespace::E:
                    tmp[i].vector1[vectorSize - j].direction = enumNamespace::W;
                    break;
                case enumNamespace::NWp:
                    tmp[i].vector1[vectorSize - j].direction = enumNamespace::SEm;
                    break;
                case enumNamespace::NWm:
                    tmp[i].vector1[vectorSize - j].direction = enumNamespace::SEp;
                    break;
                case enumNamespace::NEp:
                    tmp[i].vector1[vectorSize - j].direction = enumNamespace::SWm;
                    break;
                case enumNamespace::NEm:
                    tmp[i].vector1[vectorSize - j].direction = enumNamespace::SWp;
                    break;
                case enumNamespace::SEp:
                    tmp[i].vector1[vectorSize - j].direction = enumNamespace::NWm;
                    break;
                case enumNamespace::SEm:
                    tmp[i].vector1[vectorSize - j].direction = enumNamespace::NWp;
                    break;
                case enumNamespace::SWp:
                    tmp[i].vector1[vectorSize - j].direction = enumNamespace::NEm;
                    break;
                case enumNamespace::SWm:
                    tmp[i].vector1[vectorSize - j].direction = enumNamespace::NEp;
                    break;
            }
        }
    }
    return tmp;
}

void HPUtils::inverseHPSequence(vector<enumNamespace::HPModel> s) {
    int vectorSize = s.size();
    vector<enumNamespace::HPModel> tmp(vectorSize);
    vectorSize--;
    for (int i = s.size() - 1; i >= 0; i--) {
        switch (s[i]) {
            case enumNamespace::P:
                tmp[vectorSize - i] = enumNamespace::H;
                break;
            case enumNamespace::H:
                tmp[vectorSize - i] = enumNamespace::P;
                break;
        }
    }
}

vector<enumNamespace::HPModel> HPUtils::redFromFile(std::string filePath) {
    std::string line;
    ifstream inputFile;
    inputFile.open(filePath);

    while (!inputFile.eof()) {
        getline(inputFile, line);
        break;
    }
    vector<enumNamespace::HPModel> hpArray(line.size(), enumNamespace::P);
    for (int i = 0; i < line.size(); i++) {
        if (line[i] == 'H') {
            hpArray[i] = enumNamespace::H;
        }
    }
    return hpArray;
}

void HPUtils::initDirectionsPopulation(vector<HPItemEntity> &arrayAll) {
    srand(static_cast<unsigned int>(time(NULL)));
    vector<enumNamespace::Direction> directions;

    for (int i = 0; i < arrayAll.size(); i++) {
        for (int j = 0; j < arrayAll[0].vector1.size(); j++) {
            if (j == 0) {
                directions = generateDirectionVector();
            } else {
                directions = getDirectionVector(arrayAll[i].vector1[j].direction);
            }
            arrayAll[i].vector1[j].direction = directions[rand() % directions.size()];
            setDirectionVector(arrayAll[i].vector1[j]);
        }
    }
}

HPPoints HPUtils::getNextMove(enumNamespace::Direction direction) {
    HPPoints points;
    switch (direction) {
        case enumNamespace::N:
            points.x = 1;
            points.y = 1;
            points.z = 0;
            break;
        case enumNamespace::S:
            points.x = -1;
            points.y = -1;
            points.z = 0;
            break;
        case enumNamespace::W:
            points.x = -1;
            points.y = 1;
            points.z = 0;
            break;
        case enumNamespace::E:
            points.x = 1;
            points.y = -1;
            points.z = 0;
            break;
        case enumNamespace::NWp:
            points.x = 0;
            points.y = 1;
            points.z = 1;
            break;
        case enumNamespace::NWm:
            points.x = 0;
            points.y = 1;
            points.z = -1;
            break;
        case enumNamespace::NEp:
            points.x = 1;
            points.y = 0;
            points.z = 1;
            break;
        case enumNamespace::NEm:
            points.x = 1;
            points.y = 0;
            points.z = -1;
            break;
        case enumNamespace::SEp:
            points.x = 0;
            points.y = -1;
            points.z = 1;
            break;
        case enumNamespace::SEm:
            points.x = 0;
            points.y = -1;
            points.z = -1;
            break;
        case enumNamespace::SWp:
            points.x = -1;
            points.y = 0;
            points.z = 1;
            break;
        case enumNamespace::SWm:
            points.x = -1;
            points.y = 0;
            points.z = -1;
            break;
    }
    return points;
}

void HPUtils::checkWalk(HPItemEntity &hpItemEntity, vector<enumNamespace::HPModel> s) {
    map<Point, enumNamespace::HPModel> hash;
    hash.clear();
    HPPoints oldPoints;
    enumNamespace::Direction prevDirection;
    vector<enumNamespace::Direction> tmp;
    enumNamespace::Direction moveDirection;
    Point testPoint;

    HPPoints points(0, 0, 0);

    points = getNextMove(hpItemEntity.vector1[0].direction);
    testPoint[0] = points.x;
    testPoint[1] = points.y;
    testPoint[2] = points.z;

    hash[testPoint] = s[0];

    cout << "Point added: x:" << testPoint[0] << " y:" << testPoint[1] << " z:" << testPoint[2] << endl;

    prevDirection = hpItemEntity.vector1[0].direction;
    oldPoints = points;

    for (int i = 1; i < hpItemEntity.vector1.size(); i++) {
        moveDirection = hpItemEntity.vector1[i].direction;

        points = getNextMove(moveDirection);
        HPPoints test (points.x + oldPoints.x, points.y + oldPoints.y, points.z + oldPoints.z);

        testPoint[0] = test.x;
        testPoint[1] = test.y;
        testPoint[2] = test.z;

        if (hash.find(testPoint) == hash.end()) {
            oldPoints = test;
            hash[testPoint] = s[i];
            cout << "Point added: x:" << test.x << " y:" << test.y << " z:" << test.z << ", olde direction" << endl;
            continue;
        }

        tmp = generateDirectionVector();
        removeDirection(tmp, moveDirection);
        do {
            random_shuffle(tmp.begin(), tmp.end());

            if (tmp.size() == 0) {
                cout<<"No solution";
                system("pause");
            }
            points = getNextMove(tmp[0]); //add random picker
            moveDirection = tmp[0];

            HPPoints test(points.x + oldPoints.x, points.y + oldPoints.y, points.z + oldPoints.z);
            testPoint[0] = test.x;
            testPoint[1] = test.y;
            testPoint[2] = test.z;

            if (hash.find(testPoint) == hash.end()) {
                oldPoints = test;
                hash[testPoint] = s[i];
                hpItemEntity.vector1[i].direction = moveDirection;
                cout << "Point added: x:" << test.x << " y:" << test.y << " z:" << test.z << ", new direction" <<endl;
                break;
            } else {
                removeDirection(tmp, moveDirection);
            }
        } while (true);
    }
    cout << "FINISH :" << hash.size() <<endl;
    eval(hpItemEntity, s);
}

void HPUtils::removeDirection(vector<enumNamespace::Direction> &tmp, enumNamespace::Direction direction) {
    for (int i = 0; i < tmp.size(); i++) {
        if(direction==tmp[i]) {
            tmp.erase(tmp.begin() + i);
            break;
        }
    }
}

bool HPUtils::checkIfNeighbours(Point startPoitn, Point newPoint) {
    int forMod = 0;
    forMod = startPoitn[0] - newPoint[0];
    forMod += startPoitn[1] - newPoint[1];
    forMod += startPoitn[2] - newPoint[2];
    if (forMod % 2 != 0) {
        return false;
    }

    return abs(startPoitn[0] - newPoint[0]) <= 1 && abs(startPoitn[1] - newPoint[1]) <= 1 && abs(startPoitn[2] - newPoint[2]) <= 1;
}

void HPUtils::eval(HPItemEntity &tmp, vector<enumNamespace::HPModel> s) {
    Point starter;
    Point nexPoint;

    starter[0] = 0;
    starter[1] = 0;
    starter[2] = 0;

    for (int i = 0; i < tmp.vector1.size(); ++i) {
        starter = calcNewLocation(starter, tmp.vector1[i].direction);

        nexPoint = starter;
        for (int j = i; j < tmp.vector1.size(); ++j) {
            if(s[i]==s[j] == enumNamespace::H) {
                nexPoint = calcNewLocation(nexPoint, tmp.vector1[j].direction);
                if (checkIfNeighbours(starter, nexPoint)) {
                    tmp.fitness--;
                }
            } else {
                nexPoint = calcNewLocation(nexPoint, tmp.vector1[j].direction);
            }
        }
    }
}

Point HPUtils::calcNewLocation(Point point, enumNamespace::Direction direction) {
        switch (direction) {
            case enumNamespace::N:
                point[0]++;
                point[1]++;
                break;
            case enumNamespace::S:
                point[0]--;
                point[1]--;
                break;
            case enumNamespace::W:
                point[0]--;
                point[1]++;
                break;
            case enumNamespace::E:
                point[0]++;
                point[1]--;
                break;
            case enumNamespace::NWp:
                point[1]++;
                point[2]++;
                break;
            case enumNamespace::NWm:
                point[1]++;
                point[2]--;
                break;
            case enumNamespace::NEp:
                point[0]++;
                point[2]++;
                break;
            case enumNamespace::NEm:
                point[0]++;
                point[2]--;
                break;
            case enumNamespace::SEp:
                point[1]--;
                point[2]++;
                break;
            case enumNamespace::SEm:
                point[1]--;
                point[2]--;
                break;
            case enumNamespace::SWp:
                point[0]--;
                point[2]++;
                break;
            case enumNamespace::SWm:
                point[0]--;
                point[2]--;
                break;
        }
    return point;
}

void HPUtils::mutation(HPItemEntity &tmp) {
    int start = rand() % tmp.vector1.size();
    tmp.vector1[start].direction = enumNamespace::Direction(rand() % 12);
}

void HPUtils::corssoverMutation(vector<HPItemEntity> pop, vector<HPItemEntity> &tmpPop) {
    HPItemEntity first;
    HPItemEntity second;
    int j;

    for (int i = 0; i < pop.size(); ++i) {
        first = pop[i];
        do {
            j = rand() % pop.size();
        } while(i==j);
        second = pop[j];
        treePointCrossover(first, second);

        if (pop[i].vector1 == first.vector1 || pop[j].vector1 == first.vector1 ||
            (rand()) / (static_cast <float> (RAND_MAX / 1)) < 0.10) {

            mutation(first);
        }
        if (pop[i].vector1 == second.vector1 ||
            pop[j].vector1 == second.vector1 || (rand()) / (static_cast <float> (RAND_MAX / 1)) < 0.10) {
            mutation(second);
        }
        tmpPop.push_back(first);
        tmpPop.push_back(second);
    }
}

void HPUtils::treePointCrossover(HPItemEntity &first, HPItemEntity &second) {
    int start = rand() % first.vector1.size();
    int end = rand() % first.vector1.size() + start;

    HPItemEntity tmpFirst(first.vector1.size(), 0);
    HPItemEntity tmpSecond(first.vector1.size(), 0);

    for (int i = 0; i < first.vector1.size(); ++i) {
        if (i > start && i < end) {
            tmpFirst.vector1[i].direction = second.vector1[i].direction;
            tmpSecond.vector1[i].direction = first.vector1[i].direction;
        } else {
            tmpFirst.vector1[i].direction = first.vector1[i].direction;
            tmpSecond.vector1[i].direction = second.vector1[i].direction;
        }
    }
}

HPItemEntity HPUtils::nerest(HPItemEntity tmpItem, vector<HPItemEntity> &tmpPop) {
    for (int i = 0; i < tmpPop.size(); i++) {
        tmpPop[i].distance = 0;
        for (int j = 0; j < tmpItem.vector1.size(); j++) {
            if (tmpItem.vector1[j].direction == tmpPop[i].vector1[j].direction) {
                tmpPop[i].distance++;
            }
        }
    }

    sort(tmpPop.begin(), tmpPop.end());
    return tmpPop[0];
}

