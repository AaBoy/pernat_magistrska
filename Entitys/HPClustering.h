//
// Created by Primoz-PC on 16. 09. 2016.
//

#ifndef PERNAT_MAGISTRSKA_HPCLUSTERING_H
#define PERNAT_MAGISTRSKA_HPCLUSTERING_H
#include <vector>
#include "HPEnumDirection.h"
#include "HPItemEntity.h"

class HPClustering {
public:
    vector<vector<HPItemEntity>> static clustering(vector<HPItemEntity> p, int popSize, int subPopSize);

    vector<HPItemEntity> static nearest(vector<HPItemEntity> &t, HPItemEntity tr, int subPopSize);

    void static sortPopDistance(vector<HPItemEntity> &t);
};


#endif //PERNAT_MAGISTRSKA_HPCLUSTERING_H
