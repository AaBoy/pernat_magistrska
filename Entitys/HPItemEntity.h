//
// Created by Primoz-PC on 12. 09. 2016.
//

#ifndef PERNAT_MAGISTRSKA_HPITEMENTITY_H
#define PERNAT_MAGISTRSKA_HPITEMENTITY_H

#include <vector>
#include "HPCoordinate.h"
using namespace std;

class HPItemEntity {
public:
    double fitness;
    vector<HPCoordinate> vector1;
    int distance;
    HPItemEntity(int arraySize, double fitness);

    HPItemEntity();

    double getFitness() const;

    void setFitness(double fitness);

    bool operator<(const HPItemEntity &other) const;

    inline bool operator==(const HPItemEntity& seq) const {
        for(int i=0; i<vector1.size(); i++){
            if(vector1[i].direction != seq.vector1[i].direction) return false;
        }
        return true;
    }
};


#endif //PERNAT_MAGISTRSKA_HPITEMENTITY_H
