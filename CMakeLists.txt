cmake_minimum_required(VERSION 3.5)
project(pernat_magistrska)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(SOURCE_FILES main.cpp Utils/FirstUtil.cpp Entitys/Item.cpp Entitys/Item.cpp Entitys/HPItemEntity.cpp Entitys/HPItemEntity.h Entitys/HPCoordinate.cpp Entitys/HPCoordinate.h Entitys/HPEnumDirection.cpp Entitys/HPEnumDirection.h Entitys/HPUtils.cpp Entitys/HPUtils.h Entitys/HPClustering.cpp Entitys/HPClustering.h Entitys/HPPoints.cpp Entitys/HPPoints.h)
add_executable(pernat_magistrska ${SOURCE_FILES} Utils/FirstUtil.cpp)